
export class SectionModel
{
    ID: number;
    Name: string;
    Description: string;
    Icon: string;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class UserModel {
    Name: string;
    PhoneNumber: number;
    CountryCode: number;
    Pword: string;
    PwordVerify: string;
    Email: string;
    
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}