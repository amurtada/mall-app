//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the SqliteProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SqliteProvider {

DATABASE_FILE_NAME: string = 'data.db';
theConsole: string = "Console Messages";
private db: SQLiteObject;

  constructor(private sqlite: SQLite) {
    console.log('Hello SQLite DB Provider');
    this.createDbSchema();
    console.log(this.getConsoleMessages());
  }



createDbSchema()
{
    this.sqlite.create({
    name: this.DATABASE_FILE_NAME,
    location: 'default',
    createFromLocation: 1
  })
    .then((db: SQLiteObject) => {
      console.log('Database Created !');
      this.db = db;
      this.createTables();
    })
    .catch(e => console.log(e));
  }

  DropDatabase(){
    this.sqlite.deleteDatabase({ name: this.DATABASE_FILE_NAME, location: 'default'});
  }



  createTables()
  {
    var sql = 'create table  IF NOT EXISTS orders(rowid INTEGER PRIMARY KEY,user VARCHAR(32), product_id BIGINT, price REAL, date TEXT)'; //, quantity BIGINT
     this.db.executeSql(sql, [])
             .then((res) => 
              console.log('sqlite createTables() => Succeeded :\n res \n Executed SQL: \n' + sql + '\n')
             ).catch(e => console.log(' \n sqlite createTables() => Error: \n ' + JSON.stringify(e) +'\n'));
  }

  orderInsert(user, product_id, price) //, quantity
  {
    var sql = "INSERT INTO orders (user, product_id, price, date)  VALUES ('"+user+"', '" + product_id + "' , '"+ price + "', datetime('now'))";
    console.log('sqlite orderInsert() => sql command : ' + sql);
    
    this.db.executeSql(sql,[])
     .then(() => console.log( "\n" + 'sqlite orderInsert() => Succeeded. \n Executed SQL: \n' + sql))
     .catch(e => console.log( "\n" + 'sqlite orderInsert() =>  Error : \n' + JSON.stringify(e)));
  }

  orderCount(){
    var sql = "SELECT * FROM orders";
     
     this.db.executeSql(sql, [])
     .then((result) => {
        return result.rows.length;
     }).catch((res) => {
       return 'sqlite orderCount() => Error :'+ res;
     });
  }

   getOrders() {
          return [
            {   rowid: 1, user: 'Ali', product_id: 1, product_name: 'مازا 250مل', price: 213, date: '23-5-2018'   },
            {   rowid: 2, user: 'Hassan', product_id: 2, product_name: 'كولا 250مل', price: 213, date: '11-2-2018'   },
            {   rowid: 3, user: 'Saif', product_id: 3, product_name: 'دقيق مخصوص', price: 213, date: '15-7-2018'   },
            {   rowid: 4, user: 'Yosif', product_id: 4, product_name: 'دقيق سيقا', price: 213, date: '6-1-2018'   },
            {   rowid: 5, user: 'Siddig', product_id: 5, product_name: 'دقيق الأول', price: 213, date: '2-3-2018'   },
            {   rowid: 6, user: 'Nasir', product_id: 6, product_name: 'صلصلة الوابل', price: 213, date: '8-9-2018'   }
          ]
 }

   getOrder(rowid) {
     var sql = 'SELECT * FROM orders WHERE rowid=${rowid}';
     console.log('sqlite orderinsert() =>  sql command ' + sql);

      return this.db.executeSql(sql)
        .then(res => {
          let data;
          if(res.rows.length > 0) {
            data.rowid = res.rows.item(0).rowid;
            data.user = res.rows.item(0).user;
            data.product_id = res.rows.item(0).product_id;
            data.price = res.rows.item(0).price;
            data.date = res.rows.item(0).date;
          }
          console.log('sqlite getOrder() => Succeeded.' );
          return data || {};
        })
        .catch(e => {
          console.log('sqlite getOrder() => Error :'+ e);
          alert('sqlite getOrder()=> Error :'+ e);
        });
    
  }

  updateData() {
    var sql = 'UPDATE orders SET user=${user}, product_id=${product_id}, price=${price}  WHERE rowid=${rowid}';

    return this.db.executeSql(sql)
        .then(res => {
          console.log('sqlite updateData() => Succeeded :'+res);
              //this.navCtrl.popToRoot();
            }
          )
        .catch(e => {
          console.log('sqlite updatData() => Error :' + e);
          alert('sqlite updatData() => Error :' + e);
            }
          );
  }

  deleteOrder(rowid) {

    var sql = 'DELETE FROM orders WHERE rowid= ${rowid}';

    return this.db.executeSql(sql)
      .then(res => {
        console.log('sqlite deleteData() => Succeeded :' + res);
        this.getOrders();
      })
      .catch(e => console.log('sqlite deleteData() => Error :' + e));
  }

  deleteAllOrders()
  {
          var sql = 'DELETE FROM orders';
    return this.db.executeSql(sql)
      .then(res => {
        console.log('sqlite deleteData() => Succeeded :' + res);
        this.getOrders();
      })
      .catch(e => console.log('sqlite deleteData() => Error :' + e));
  }

  getConsoleMessages(){
    return this.theConsole;
  }
  
}
 