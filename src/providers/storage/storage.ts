
//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';


/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageProvider {

  constructor(public storage: Storage) {
    console.log('Hello Local Storage Provider');
  }


  setData(key: string, value: string)
  {
    this.storage.set(key,value);
  }

  getData(key: string) 
  {
    this.storage.get(key).then((val) => {
        console.log('Your '+ key + ' is : ' + val);
        return val;
    });
  }

  removeData(key: string)
  {
    this.storage.remove(key)
      .then((res) => { console.log('Removed Successfully. '+ res) })
      .catch((res) => { console.log('Removed Failed. '+ res) });
  }

  clearStorage()
  {
    this.storage.clear()
      .then((res) => { console.log('Cleared Successfully. '+ res) })
      .catch((res) => { console.log('Clearing Failed. '+ res) });
  }

  count(){
    this.storage.length()
          .then((res) => { console.log(' Succeed. '+ res); return length; })
          .catch((res) => { console.log('Failed. '+ res); return -1;   });
  }

}
