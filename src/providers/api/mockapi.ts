import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable  } from 'rxjs';
import { catchError, map, tap  } from 'rxjs/operators';
import  'rxjs/add/operator/catch';


import { UserModel, SectionModel } from '../models/models';




/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  //apiUrl = 'http://192.168.43.102:45455/api';
  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }

  // private extractData(res){
  //   let body = res;
  //   return body || {};
  // }

  getSections() {
       
    return [
      {"id":1,"name":"المواد الغذائية","description":"كل ما يتعلق بالمواد الغذائية","icon":"../assets/imgs/609.jpg"},{"id":2,"name":"السكريات","description":"كل ما يتعلق بالسكريات","icon":"../assets/imgs/1819.jpg"},{"id":3,"name":"المواد التموينية","description":"كل ما يتعلق بالمواد التموينية","icon":"../assets/imgs/background.jpg"}];
      
  }

  public getProductCategories(){

    return [{"id":1,"name":"الدقيق","section_ID":1,"section":null,"products":[{"id":1,"name":"دقيق زادنا","category_ID":1,"note":"1 كيلوجرام","icon":"../assets/imgs/zadna.jpg","price":25},{"id":2,"name":"دقيق الأول","category_ID":1,"note":"750 جرام","icon":null,"price":20},{"id":6,"name":"دقيق مخصوص","category_ID":1,"note":"1 كيلو جرام","icon":"../assets/imgs/makhsous.jpg","price":30}]},{"id":2,"name":"الصلصة","section_ID":1,"section":null,"products":[]},{"id":3,"name":"الأرز","section_ID":3,"section":null,"products":[{"id":4,"name":"أرز الوابل","category_ID":3,"note":"500 جرام","icon":null,"price":15},{"id":5,"name":"أرز الفراشة","category_ID":3,"note":"500 جرام","icon":"../assets/imgs/farasha.png","price":17}]},{"id":4,"name":"السكر","section_ID":2,"section":null,"products":[{"id":8,"name":"سكر صافي","category_ID":4,"note":"1كيلو جرام","icon":"../assets/imgs/safi.jpg","price":40}]}];
  }


  generateCode(mobile: any)
  {
    return 1;
  }

  verifyCode(code: string, mobile: string)
  {
      return 1;
  }

    
  

}
