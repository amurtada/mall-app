
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

//applications Pages
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { IndexPage } from '../pages/index/index';
import { DevicesPage } from '../pages/devices/devices';
import { RegisterPage } from '../pages/register/register';
import { CodeVerifyPage } from '../pages/code-verify/code-verify';
import { PurchasePage } from '../pages/purchase/purchase';
import { ContactPage } from '../pages/contact/contact';


//  For Smooth Transitions Between Pages | Step 2 Add Into providers
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { ApiProvider } from '../providers/api/mockapi';

// For Sending and Receiving Requests
import { HttpClientModule } from '@angular/common/http';

//To Allow Using ngmodel
import { FormsModule } from '@angular/forms';

//To Using Sqlite
import { SQLite } from '@ionic-native/sqlite';
import { SqliteProvider } from '../providers/sqlite/sqlite';
import { IonicStorageModule } from '@ionic/storage';
import { StorageProvider } from '../providers/storage/storage';





@NgModule({

  declarations: 
  [
    MyApp,
    HomePage,
    ListPage,
    IndexPage,
    DevicesPage,
    RegisterPage,
    CodeVerifyPage,
    PurchasePage,
    ContactPage
  ],

  imports: 
  [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    IndexPage,
    DevicesPage,
    RegisterPage,
    CodeVerifyPage,
    PurchasePage,
    ContactPage
  ],

  providers: [

    StatusBar,
    SplashScreen,
    NativePageTransitions,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    SQLite,
    SqliteProvider,
    StorageProvider

  ]


})
export class AppModule {}
