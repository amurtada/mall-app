import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IndexPage } from '../index/index';
import { ApiProvider } from '../../providers/api/mockapi';
import { UserModel } from '../../providers/models/models';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  
  User: UserModel ={
       Name: '',
       PhoneNumber: null,
       CountryCode: 249,
       Pword: '',
       PwordVerify: '',
       Email: '' 
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: ApiProvider) {
    this.User.PhoneNumber =  navParams.get('mobile');
  }

  SaveData(User: UserModel){
      //this.db.addUser(User);
  }

}
