
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// For Smooth Transitions Between Pages
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { DevicesPage } from '../devices/devices';
import { SectionModel } from '../../providers/models/models';
import { ApiProvider } from '../../providers/api/mockapi';
import { Observable } from  'rxjs/Observable';


@IonicPage()
@Component({
  selector: 'page-index',
  templateUrl: 'index.html',
})


export class IndexPage {

  sections: any;

  sectionTest: SectionModel = {
    ID : null,
    Name : 'المواد الغذائية',
    Description : 'كل ما يخص المواد الغذائية',
    Icon : '../assets/imgs/background.jpg', 
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: ApiProvider , private nativePageTransitions: NativePageTransitions) 
  {

  }

  ngOnInit() {
    this.getSections();
  }

  getSections(){
      console.log("loading Sections...");
      let data = this.db.getSections();
        console.log(data);
        this.sections = data;

     }
  
  GoToSub() {

    let options: NativeTransitionOptions = 
    {

       direction: 'up', // Left + Right + Up + Down, Default Up

       duration: 500, // In Milliseconds(ms), Default 400

       slowdownfactor: 3,
       slidePixels: 20,

       iosdelay: 100,
       androiddelay: 150,
       winphonedelay :150,

       fixedPixelsTop: 0,

       fixedPixelsBottom: 60

      };
   
    this.nativePageTransitions.slide(options)

      .then( function() { alert("Success")})
      .catch(function() { alert("Error")});
   
      this.navCtrl.push(DevicesPage);
   }
}
