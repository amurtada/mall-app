import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/mockapi';
import { FormsModule } from '@angular/forms';
import { RegisterPage } from '../register/register';

/**
 * Generated class for the CodeVerifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-code-verify',
  templateUrl: 'code-verify.html',
})
export class CodeVerifyPage {

  mobile = '';
  

  constructor(public navCtrl: NavController, public db: ApiProvider, public navParams: NavParams) {
    this.mobile = navParams.get('mobile');
    console.log('from codeVerify, mobile : ' + this.mobile);
  }
    
  verifyCode(theCode: any)
  {
    var res = 0;
    console.log('from code-Verify : Code is ' + theCode);
    var myPromise = this.db.verifyCode(theCode, this.mobile);
    myPromise.then((result) => {
      console.log("Result = " + result);
      if(result == 1)
      {
        this.navCtrl.push(RegisterPage, { 'mobile': this.mobile});
      }else{
        alert("Code Error.");
      } 
    }).catch((result) => {
        alert("Network Error. Try Again");
        console.log("Verify Code Failed. Try again");
        console.log(result);
      });

    
  }

  login(){
    alert('user Logged In !!');

  }

}
