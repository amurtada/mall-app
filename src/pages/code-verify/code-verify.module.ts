import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CodeVerifyPage } from './code-verify';

@NgModule({
  declarations: [
    CodeVerifyPage,
  ],
  imports: [
    IonicPageModule.forChild(CodeVerifyPage),
  ],
})
export class CodeVerifyPageModule {}
