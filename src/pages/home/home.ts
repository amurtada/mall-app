import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { CodeVerifyPage } from '../code-verify/code-verify';
import { ApiProvider } from '../../providers/api/mockapi';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public db: ApiProvider) {

  }

  GoToReg()
  {

    this.navCtrl.push(RegisterPage);
  }

  getCode(countryCode: string,mobileNumber : string)
  {
     var f: any;
      var mobile = countryCode + mobileNumber;
      var myPromise = this.db.generateCode(mobile);
      var res = myPromise.then((result) => {
        console.log("Generate Code Succeeded. ");
        console.log('Result = '+ result);
        if(result == 1)
          this.navCtrl.push(CodeVerifyPage, { 'mobile': mobile});
        else
          alert("Error in Mobile Number.");
      }).catch((result) => {
        alert("Network Error. Try Again");
        console.log("Generate Code Failed. Try again");
        console.log(result);
      });

  }
}
