
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/mockapi';
import { SqliteProvider } from '../../providers/sqlite/sqlite';
import { PurchasePage } from '../purchase/purchase';


@IonicPage()

@Component({
  selector: 'page-devices',
  templateUrl: 'devices.html',
})
export class DevicesPage 
{
  categories: any = [];

  pet : string ;
  cartTotal: number;
  count: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: ApiProvider, public sqlite: SqliteProvider) 
  {
    this.cartTotal = 0;
  }

  ngOnInit(){
    this.getProductsCategories();
  }

  getProductsCategories(){
    this.categories = [];
    let data = this.db.getProductCategories();
      console.log(data);
      this.categories = data;
  }

  addToCart(id, price){
    //step 1: make the purchase order
    //this.sqlite.orderInsert('ahmed', id,price);
    console.log(this.sqlite.getConsoleMessages());
    console.log(' devices => addToCart');
    //step 2: update the values in backend
    this.cartTotal += price;
    
  }

   removefromCart(id, price){
    //step 1: make the purchase order
    //this.sqlite.deleteOrder(rowid);
    //step 2: update the values in backend
    this.cartTotal -= price;
    console.log(' devices =>  RemoveFromCart');
  }

  completeOrder(){
    this.navCtrl.push(PurchasePage);
  }

}