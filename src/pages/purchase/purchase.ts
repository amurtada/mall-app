import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SqliteProvider } from '../../providers/sqlite/sqlite';

/**
 * Generated class for the PurchasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-purchase',
  templateUrl: 'purchase.html',
})
export class PurchasePage {
  data = [];
  messages: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sqlite: SqliteProvider) {
  }

ngOnInit()
  {
    this.getPurchases();
  }
  getPurchases(){
     
       console.log('loading orders');
       this.data = this.sqlite.getOrders();
       console.log('loaded orders');
  }
}
